<?php
/**
 * @file
 * CCK-hooks for piwik_stats module.
 */

/**
 * Implements hook_field_info().
 *
 * Provides the description of the field.
 */
function piwik_stats_field_info() {
  return array(
    'piwik_stats' => array(
      'label' => t('Piwik Statistical Field'),
      'description' => t('Holds piwik statistical information of a node.'),
    ),
  );
}

/**
 * Field schema.
 *
 * This array have to be changed if changed piwik_stats_definitions().
 */
function piwik_stats_field_schema() {
  return array(
    'nb_visits' => array(
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'sortable' => TRUE,
      'views' => array(
        'filter' => array(
          'handler' => 'content_handler_filter_numeric',
        ),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),
    ),
    'nb_hits' => array(
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'sortable' => TRUE,
      'views' => array(
        'filter' => array(
          'handler' => 'content_handler_filter_numeric',
        ),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),
    ),
    'entry_nb_visits' => array(
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'sortable' => TRUE,
      'views' => array(
        'filter' => array(
          'handler' => 'content_handler_filter_numeric',
        ),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),
    ),
    'entry_nb_actions' => array(
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'sortable' => TRUE,
      'views' => array(
        'filter' => array(
          'handler' => 'content_handler_filter_numeric',
        ),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),
    ),
    'entry_sum_visit_length' => array(
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'sortable' => TRUE,
      'views' => array(
        'filter' => array(
          'handler' => 'content_handler_filter_numeric',
        ),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),
    ),
    'entry_bounce_count' => array(
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'description' => 'Number of visits that started on this node and bounced (viewed only one page).',
      'sortable' => TRUE,
      'views' => array(
        'filter' => array(
          'handler' => 'content_handler_filter_numeric',
        ),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),
    ),
    'exit_nb_visits' => array(
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'sortable' => TRUE,
      'views' => array(
        'filter' => array(
          'handler' => 'content_handler_filter_numeric',
        ),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),
    ),
    'sum_time_spent' => array(
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'sortable' => TRUE,
      'views' => array(
        'filter' => array(
          'handler' => 'content_handler_filter_numeric',
        ),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),
    ),
    'bounce_rate' => array(
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'sortable' => TRUE,
      'views' => array(
        'filter' => array(
          'handler' => 'content_handler_filter_numeric',
        ),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),
    ),
    'exit_rate' => array(
      'type' => 'int',
      'not null' => TRUE,
      'default' => 0,
      'sortable' => TRUE,
      'views' => array(
        'filter' => array(
          'handler' => 'content_handler_filter_numeric',
        ),
        'field' => array(
          'handler' => 'views_handler_field_numeric',
        ),
      ),
    ),
  );
}

/**
 * Implements hook_field_settings().
 */
function piwik_stats_field_settings($op, $field) {
  switch ($op) {
    case 'database columns':
      // DB fields.
    return piwik_stats_field_schema();
    case 'views data':
      $data = content_views_field_views_data($field);
      // $db_info = content_database_info($field);
      $table = content_views_tablename($field);

      $piwik_stats_field_schema = piwik_stats_field_schema();

      foreach ($piwik_stats_field_schema as $name => $info) {
        if ($views = $info['views']) {
          $alias = $field['field_name'] . '_' . $name;
          foreach (array('field', 'filter') as $key) {
            if (isset($views[$key]) && isset($data[$table][$alias][$key])) {
              $data[$table][$alias][$key] = array_merge($data[$table][$alias][$key], $views[$key]);
            }
          }
        }
      }
      break;
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function piwik_stats_field_formatter_info() {
  $formatters = array();

  $formatters['piwik_stats_table'] = array(
    'label' => t('Statistics table'),
    'field types' => array('piwik_stats'),
  );

  foreach (piwik_stats_definitions() as $name => $info) {
    $formatters['piwik_stats_' . $name] = array(
      'label' => t('Statistics @title', array('@title' => $info['title'])),
      'field types' => array('piwik_stats'),
    );
  }

  return $formatters;
}

/**
 * Implements hook_theme().
 */
function piwik_stats_theme($existing, $type, $theme, $path) {
  $formatters = array();

  $formatters['piwik_stats_formatter_piwik_stats_table'] = array(
    'arguments' => array('element' => NULL),
    'file' => 'piwik_stats.theme.inc',
  );

  $formatters['piwik_stats_hidden'] = array(
    'arguments' => array('element' => NULL),
    'file' => 'piwik_stats.theme.inc',
  );

  foreach (piwik_stats_definitions() as $name => $info) {
    $formatters['piwik_stats_formatter_piwik_stats_' . $name] = array(
      'arguments' => array('element' => NULL),
      'file' => 'piwik_stats.theme.inc',
    );
  }

  return $formatters;
}

/**
 * Implements hook_content_is_empty().
 */
function piwik_stats_content_is_empty($item, $field) {
  return empty($item);
}

/**
 * Implements hook_widget_info().
 */
function piwik_stats_widget_info() {
  return array(
    'piwik_stats_hidden' => array(
      'label' => t('None'),
      'field types' => array('piwik_stats'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array(
        'default value' => CONTENT_CALLBACK_DEFAULT,
      ),
    ),
  );
}

/**
 * Implements hook_widget().
 */
function piwik_stats_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  $element = array(
    '#title' => $field['widget']['label'],
    '#type' => $field['widget']['type'],
    '#default_value' => isset($items[$delta]) ? $items[$delta] : NULL,
  );

  return $element;
}

/**
 * Implements hook_elements().
 */
function piwik_stats_elements() {
  $elements = array(
    'piwik_stats_hidden' => array(
      '#input' => TRUE,
      '#columns' => array_keys(piwik_stats_definitions()),
      '#process' => array('_piwik_stats_field_process'),
      '#delta' => 0,
    ),
  );

  return $elements;
}

/**
 * Process function for piwik_stats field widget.
 */
function _piwik_stats_field_process($element, $edit, &$form_state, $form) {
  $field_name = $element['#field_name'];
  $field = $form['#field_info'][$field_name];
  $field_keys  = $element['#columns'];

  foreach ($field_keys as $key => $val) {
    $value = isset($element['#value'][$val]) ? $element['#value'][$val] : 0;

    $element[$val] = array(
      '#type' => 'hidden',
      '#default_value' => isset($element['#default_value'][$val]) ? $element['#default_value'][$val] : 0,
      '#title' => $element['#title'],
      '#description' => $element['#description'],
      '#required' => $element['#required'],
      '#field_name' => $element['#field_name'],
      '#type_name' => $element['#type_name'],
      '#delta' => $element['#delta'],
      '#columns' => $element['#columns'],
    );
  }
  // Make sure we don't wipe out element validation added elsewhere.
  if (empty($element['#element_validate'])) {
    $element['#element_validate'] = array();
  }

  return $element;
}

/**
 * Implements hook_widget_settings().
 */
function piwik_stats_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      // Add field settings for statistic period.
      $form['period'] = array(
        '#type' => 'select',
        '#title' => t('Period'),
        '#default_value' => isset($widget['period']) ? $widget['period'] : 'day',
        '#options' => array(
          'day' => t('Day'),
          'week' => t('Week'),
          'month' => t('Month'),
          'year' => t('Year'),
        ),
        '#required' => TRUE,
        '#description' => t('The period of the requested statistics.'),
      );

      // Add field settings for statistic update frequency.
      $form['statistics_update'] = array(
        '#type' => 'select',
        '#title' => t('Statistic update frequency'),
        '#default_value' => isset($widget['statistics_update']) ? $widget['statistics_update'] : 1440,
        '#options' => array(
          60 => '1 ' . t('hour'),
          720 => '12 ' . t('hours'),
          1440 => '1 ' . t('day'),
        ),
        '#required' => TRUE,
        '#description' => t('How often statistics have to be synchronized with Piwik server.'),
      );

      // Add field settings for amount nodes for reacalculate.
      $form['nodes_amount'] = array(
        '#type' => 'select',
        '#title' => t('Recalculate statistics for N last nodes of this type'),
        '#default_value' => isset($widget['nodes_amount']) ? $widget['nodes_amount'] : 100,
        '#options' => array(
          100 => 100,
          500 => 500,
          1000 => 1000,
          10000 => 10000,
          'all' => t('All'),
        ),
        '#required' => TRUE,
        '#description' => t('Statistics for how much nodes have to be recalculated.'),
      );

      $form['xml_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Piwik XML settings'),
        '#description' => t("Request only URL's where selected parameter is greater than selected value"),
        '#weight' => 3,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );

      $fields = array();
      foreach (piwik_stats_definitions() as $key => $val) {
        $fields[$key] = $val['title'];
      }

      // Field which value have to be filtered.
      $form['xml_settings']['filter_field'] = array(
        '#type' => 'select',
        '#title' => t('Field'),
        '#description' => t("Field which value have to be filtered."),
        '#default_value' => isset($widget['filter_field']) ? $widget['filter_field'] : 'nb_hits',
        '#options' => $fields,
        '#required' => TRUE,
      );

      $form['xml_settings']['filter_value'] = array(
        '#type' => 'textfield',
        '#title' => t('Value'),
        '#description' => t("Minimal value for the field, selected above."),
        '#default_value' => isset($widget['filter_value']) ? $widget['filter_value'] : 10,
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
      );

    return $form;
    case 'save':
    return array(
      'period',
      'statistics_update',
      'nodes_amount',
      'filter_field',
      'filter_value',
    );
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Remove some unneeded stuff from the field config.
 * Clone of the function piwik_stats_form_field_ui_field_edit_form_alter
 * from D7 version
 *
 * Form ID: form_content_field_edit (piwik_stats widget settings form).
 */
function piwik_stats_form_content_field_edit_form_alter(&$form, &$form_state) {
  if ($form['#field']['type'] == 'piwik_stats') {
    $form['field']['required']['#default_value'] = FALSE;
    $form['field']['required']['#attributes'] = array('disabled' => TRUE);

    $form['field']['multiple']['#default_value'] = 0;
    $form['field']['multiple']['#attributes'] = array('disabled' => TRUE);

    unset($form['widget']['default_value']);
    unset($form['widget']['default_value_php']);
    unset($form['widget']['default_value_fieldset']);
    unset($form['widget']['description']);
  }
}
