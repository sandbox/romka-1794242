<?php
/**
 * @file
 * Theme functions for piwik_stats field formatters.
 */

/**
 * Theme function for piwik_stats_table formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_table($element) {
  $output = '';
  $header = array(t('Field'), t('Value'));
  $rows = array();

  foreach ($element['#item'] as $name => $value) {
    if ($name != '#delta') {
      $rows[] = array($name, $value);
    }
  }

  $output .= theme('table', $header, $rows);
  return $output;
}

/**
 * Theme function for piwik_stats_nb_visits formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_nb_visits($element) {
  return $element['#item']['nb_visits'];
}

/**
 * Theme function for piwik_stats_nb_hits formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_nb_hits($element) {
  return $element['#item']['nb_hits'];
}

/**
 * Theme function for piwik_stats_entry_nb_visits formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_entry_nb_visits($element) {
  return $element['#item']['entry_nb_visits'];
}

/**
 * Theme function for piwik_stats_entry_nb_actions formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_entry_nb_actions($element) {
  return $element['#item']['entry_nb_actions'];
}

/**
 * Theme function for piwik_stats_entry_sum_visit_length formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_entry_sum_visit_length($element) {
  return $element['#item']['entry_sum_visit_length'];
}

/**
 * Theme function for piwik_stats_entry_bounce_count formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_entry_bounce_count($element) {
  return $element['#item']['entry_bounce_count'];
}

/**
 * Theme function for piwik_stats_exit_nb_visits formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_exit_nb_visits($element) {
  return $element['#item']['exit_nb_visits'];
}

/**
 * Theme function for piwik_stats_sum_time_spent formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_sum_time_spent($element) {
  return $element['#item']['sum_time_spent'];
}

/**
 * Theme function for piwik_stats_bounce_rate formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_bounce_rate($element) {
  return $element['#item']['bounce_rate'];
}

/**
 * Theme function for piwik_stats_exit_rate formatter.
 */
function theme_piwik_stats_formatter_piwik_stats_exit_rate($element) {
  return $element['#item']['exit_rate'];
}

/**
 * Theme function for piwik_stats_hidden widget.
 */
function theme_piwik_stats_hidden($element) {
  return $element['#children'];
}
