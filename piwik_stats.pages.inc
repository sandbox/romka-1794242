<?php
/**
 * @file
 * Some page callbacks for piwik_stats module.
 */

/**
 * Page callback to show statistical information on local task tabs.
 */
function piwik_stats_tab_page($node) {
  $output = '';
  $attached_fields = array();
  $header = array(t('Field'), t('Value'));

  if (!function_exists('content_field_instance_read')) {
    module_load_include('inc', 'content', 'includes/content.crud');
  }

  if (function_exists('content_field_instance_read')) {
    $fields = content_field_instance_read();
  }

  foreach ($fields as $field) {
    if ($field['type'] == 'piwik_stats') {
      if ($node->type == $field['type_name']) {
        $attached_fields[] = $field;
      }
    }
  }

  foreach ($attached_fields as $key => $field) {
    $rows = array();
    if (isset($node->$field['field_name'])) {
      $field_data = $node->$field['field_name'];
      foreach ($field_data[0] as $name => $value) {
        $rows[] = array($name, $value);
      }
      $output .= t('Statistics for period: %period', array('%period' => $field['widget']['period']));
      $output .= theme('table', $header, $rows);
    }
  }

  return $output;
}
